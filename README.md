## Config database: MySQL
## username: root
## password: 123456789
## host: localhost
## port: 3306
## database: todo

<!-- Init project -->
## npm install
## npm start


## GET list: http://localhost:1337/list



## POST create: http://localhost:1337/create
{
    name: ...
}



## POST update: http://localhost:1337/update
{
    id: ...,
    name: ...,
    status: true/false
}



## POST delete: http://localhost:1337/delete
{
    id: ...
}