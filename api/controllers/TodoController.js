/**
 * TodoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  list: async (req, res) => {
    try {
      const data = await Todo.find({});
      return res.json({ todos: data });
    } catch (error) {
      console.log(error);
    }
  },
  create: async (req, res) => {
    try {
      const data = req.body;
      if (data.name !== '') {
        const value = await Todo.create({
          name: data.name,
          status: false
        });
        return res.json({ message: 'Tạo thành công', todo: value });
      }
    } catch (error) {
      console.log(error);
    }
  },
  update: async (req, res) => {
    try {
      const data = req.body;
      if (data.name !== '') {
        const todo = await Todo.update({ id: data.id })
          .set({
            name: data.name,
            status: data.status
          });
        return res.json({ message: 'Cập nhật thành công', todo: todo });
      }
    } catch (error) {
      console.log(error);
    }
  },
  delete: async (req, res) => {
    try {
      await Todo.destroy({ id: req.body.id });
      return res.json({ message: 'Xóa thành công' });
    } catch (error) {
      console.log(error);
    }
  }
};

